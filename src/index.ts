function greeter(person: string) {
    return "Hello, " + person;
}

let user = "Jane User";

console.log(greeter(user))

const num: any = 1;
let list: string = num;


let someValue: any = "this is a string";
console.log((someValue as string).length)

interface SquareConfig {
    color?: string;
    width?: number;
    [propName: string]: any
}

function createSquare(config: SquareConfig): { color: string; ares: number} {
    return {
        color: config.color,
        ares: config.width * config.width
    }
}
let mySquare = createSquare({ colour: "red", width: 100 } )


// 接口-函数
interface AddFunc {
    add(a: number, b: number): number
}

let add : AddFunc = (a: number, b: number) => {
    return a + b;
};
